﻿namespace TaskableApp.Abstract
{
    public interface INamedFileEntry
    {
        string Name { get; set; }
        string Path { get; set; }
    }
}
